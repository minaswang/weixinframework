<?php

/**
 * 伪造请求数据类型为voice的xml对象.
 * @author:minas
 * @date:2014-05-13
 *
 */

class VoiceRequest extends AbstractRequest {

	private $voiceXmlObj; 

	function __construct($postData='')
	{
		$this->createXmlObj($postData);	
	}

	private function createXmlObj($postData='')
	{
		$ToUser = isset($postData['toUser']) ?  $postData['toUser'] : "fakeToUser";
		$FromUser = isset($postData['fromUser']) ? $postData['fromUser'] : "fakeFromUser";
		$CreateTime = isset($postData['createTime']) ? $postData['createTime'] : time();
		$MediaId = isset($postData['mediaId']) ? $postData['mediaId'] : "fakeMediaId";
		$Format = isset($postData['format']) ? $postData['format'] : "fakeFormat";
		$MessageId = isset($postData['msgId']) ? $postData['msgId'] : "fakeMessageId";
		$xmlstring = "<xml>
 					  <ToUserName><![CDATA[". $ToUser ."]]></ToUserName>
                      <FromUserName><![CDATA[". $FromUser ."]]></FromUserName> 
                      <CreateTime>". $CreateTime ."</CreateTime>
                      <MsgType><![CDATA[voice]]></MsgType>
                      <MediaId><![CDATA[". $MediaId  ."]]></MediaId>
                      <Format><![CDATA[". $Format ."]]></Format>
                      <MsgId>". $MessageId ."</MsgId>
                      </xml>";

		$xml = simplexml_load_string($xmlstring,'SimpleXMLElement', LIBXML_NOCDATA);

		$this->voiceXmlObj = $xml;
	}
	public function getXmlObj()
	{
		return $this->voiceXmlObj;
	}



}
