<?php

/**
 * 伪造请求数据类型为text的xml对象.
 * @author:minas
 * @date:2014-05-13
 *
 */
require_once("AbstractRequest.php");

class TextRequest extends AbstractRequest {

	private $textXmlObj; 

	function __construct($postData='')
	{
		$this->createXmlObj($postData);	
	}

	private function createXmlObj($postData='')
	{
		$ToUser = isset($postData['toUser']) ?  $postData['toUser'] : "fakeToUser";
		$FromUser = isset($postData['fromUser']) ? $postData['fromUser'] : "fakeFromUser";
		$CreateTime = isset($postData['createTime']) ? $postData['createTime'] : time();
		$Content = isset($postData['content']) ? $postData['content'] : "fakeContent";
		$MsgId = isset($postData['msgId']) ? $postData['msgId'] : "fakeMsgId";
		$xmlstring = "<xml>
 					  <ToUserName><![CDATA[". $ToUser ."]]></ToUserName>
                      <FromUserName><![CDATA[". $FromUser ."]]></FromUserName> 
                      <CreateTime>". $CreateTime ."</CreateTime>
                      <MsgType><![CDATA[text]]></MsgType>
                      <Content><![CDATA[". $Content ."]]></Content>
                      <MsgId>". $MsgId ."</MsgId>
                      </xml>";

		$xml = simplexml_load_string($xmlstring,'SimpleXMLElement', LIBXML_NOCDATA);

		$this->textXmlObj = $xml;
	}

	public function getXmlObj()
	{
		return $this->textXmlObj;
	}

}
