<?php
/**
 * 请求的抽象类
 * @author: minas
 * @date  : 2014-05-13
 *
 */

abstract class AbstractRequest {

	public abstract function getXmlObj();

}
