<?php

/**
 * 微信消息类工厂.
 * @author : minas
 * @date   : 2014-05-12
 *
 **/
require_once("TextRequest.php");
require_once("ImageRequest.php");
require_once("VoiceRequest.php");
require_once("VideoRequest.php");
require_once("LocationRequest.php");
require_once("LinkRequest.php");

class RequestFactory {

	public static function createRequest($request )
	{
		switch ($request['type'] ) {
			case 'text':
				return new TextRequest($request);
				break;
			case 'image':
				return new ImageRequest($request);
				break;
			case 'voice':
				return new VoiceRequest($request);
				break;
			case 'video':
				return new VideoRequest($request);
				break;
			case 'location':
				return new LocationRequest($request);
				break;
			case 'link' :
				return new LinkRequest($request);
				break;
			default:
				return false;
				break;
		}
	} 




}