<?php

/**
 * 伪造请求数据类型为image的xml对象.
 * @author:minas
 * @date:2014-05-13
 *
 */

class VideoRequest extends AbstractRequest {

	private $videoXmlObj; 

	function __construct($postData='')
	{
		$this->createXmlObj($postData);	
	}

	private function createXmlObj($postData='')
	{
		$ToUser = isset($postData['toUser']) ?  $postData['toUser'] : "fakeToUser";
		$FromUser = isset($postData['fromUser']) ? $postData['fromUser'] : "fakeFromUser";
		$CreateTime = isset($postData['createTime']) ? $postData['createTime'] : time();
		$MediaId = isset($postData['mediaId']) ? $postData['mediaId'] : "fakeMediaId";
		$ThumbMediaId = isset($postData['thumbMediaId']) ? $postData['thumbMediaId'] : "fakeThumbMediaId";
		$MessageId = isset($postData['msgId']) ? $postData['msgId'] : "fakeMessageId";
		$xmlstring = "<xml>
 					  <ToUserName><![CDATA[". $ToUser ."]]></ToUserName>
                      <FromUserName><![CDATA[". $FromUser ."]]></FromUserName> 
                      <CreateTime>". $CreateTime ."</CreateTime>
                      <MsgType><![CDATA[video]]></MsgType>
                      <MediaId><![CDATA[". $MediaId  ."]]></MediaId>
                      <ThumbMediaId><![CDATA[". $ThumbMediaId ."]]></ThumbMediaId>
                      <MsgId>". $MessageId ."</MsgId>
                      </xml>";

		$xml = simplexml_load_string($xmlstring,'SimpleXMLElement', LIBXML_NOCDATA);

		$this->videoXmlObj = $xml;
	}
	public function getXmlObj()
	{
		return $this->videoXmlObj;
	}


}
