<?php

/**
 * 伪造请求数据类型为Location的xml对象.
 * @author:minas
 * @date:2014-05-13
 *
 */

class locationRequest extends AbstractRequest {

	private $locationXmlObj; 

	function __construct($postData='')
	{
		$this->createXmlObj($postData);	
	}

	private function createXmlObj($postData='')
	{
		$ToUser = isset($postData['toUser']) ?  $postData['toUser'] : "fakeToUser";
		$FromUser = isset($postData['fromUser']) ? $postData['fromUser'] : "fakeFromUser";
		$CreateTime = isset($postData['createTime']) ? $postData['createTime'] : time();
		$Location_X = isset($postData['locationX']) ? $postData['locationX'] : "fakeLocation_X";
		$Location_Y = isset($postData['locationY']) ? $postData['locationY'] : "fakeLocation_Y";
		$Scale = isset($postData['scale']) ? $postData['scale'] : "fakeScale";
		$Label = isset($postData['label']) ? $postData['label'] : "fakeLabel";
		$MessageId = isset($postData['msgId']) ? $postData['msgId'] : "fakeMessageId";
		
		$xmlstring = "<xml>
 					  <ToUserName><![CDATA[". $ToUser ."]]></ToUserName>
                      <FromUserName><![CDATA[". $FromUser ."]]></FromUserName> 
                      <CreateTime>". $CreateTime ."</CreateTime>
                      <MsgType><![CDATA[location]]></MsgType>
                      <Location_X>".$Location_X."</Location_X>
					  <Location_Y>".$Location_Y."</Location_Y>
					  <Scale>". $Scale ."</Scale>
					  <Label><![CDATA[". $Label ."]]></Label>
                      <MsgId>". $MessageId ."</MsgId>
                      </xml>";

		$xml = simplexml_load_string($xmlstring,'SimpleXMLElement', LIBXML_NOCDATA);

		$this->locationXmlObj = $xml;
	}
	public function getXmlObj()
	{
		return $this->locationXmlObj;
	}



}
