<?php

/**
 * 伪造请求数据类型为image的xml对象.
 * @author:minas
 * @date:2014-05-13
 *
 */


require_once("AbstractRequest.php");

class ImageRequest extends AbstractRequest {

	private $imageXmlObj; 

	function __construct($postData='')
	{
		$this->createXmlObj($postData);	
	}

	private function createXmlObj($postData='')
	{
		$ToUser = isset($postData['toUser']) ?  $postData['toUser'] : "fakeToUser";
		$FromUser = isset($postData['fromUser']) ? $postData['fromUser'] : "fakeFromUser";
		$CreateTime = isset($postData['createTime']) ? $postData['createTime'] : time();
		$PicUrl = isset($postData['picUrl']) ? $postData['picUrl'] : "fakePicUrl";
		$MediaId = isset($postData['mediaId']) ? $postData['mediaId'] : "fakeMediaId";
		$MessageId = isset($postData['msgId']) ? $postData['msgId'] : "fakeMessageId";
		$xmlstring = "<xml>
 					  <ToUserName><![CDATA[". $ToUser ."]]></ToUserName>
                      <FromUserName><![CDATA[". $FromUser ."]]></FromUserName> 
                      <CreateTime>". $CreateTime ."</CreateTime>
                      <MsgType><![CDATA[image]]></MsgType>
                      <PicUrl><![CDATA[".$PicUrl ."]]></PicUrl>
                      <MediaId><![CDATA[". $MediaId  ."]]></MediaId>
                      <MsgId>". $MessageId ."</MsgId>
                      </xml>";

		$xml = simplexml_load_string($xmlstring,'SimpleXMLElement', LIBXML_NOCDATA);

		$this->imageXmlObj = $xml;
	}

	public function getXmlObj()
	{
		return $this->imageXmlObj;
	}


}
