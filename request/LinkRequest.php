<?php

/**
 * 伪造请求数据类型为Link的xml对象.
 * @author:minas
 * @date:2014-05-13
 *
 */

class LinkRequest extends AbstractRequest {

	private $linkXmlObj; 

	function __construct($postData='')
	{
		$this->createXmlObj($postData);	
	}

	private function createXmlObj($postData='')
	{
		$ToUser = isset($postData['toUser']) ?  $postData['toUser'] : "fakeToUser";
		$FromUser = isset($postData['fromUser']) ? $postData['fromUser'] : "fakeFromUser";
		$CreateTime = isset($postData['createTime']) ? $postData['createTime'] : time();
		$Title = isset($postData['title']) ? $postData['title'] : "fakeTitle";
		$Description = isset($postData['description']) ? $postData['description'] : "fakeDescription";
		$Url = isset($postData['url']) ? $postData['url'] : "fakeUrl";
		$MessageId = isset($postData['msgId']) ? $postData['msgId'] : "fakeMessageId";
		
		$xmlstring = "<xml>
 					  <ToUserName><![CDATA[". $ToUser ."]]></ToUserName>
                      <FromUserName><![CDATA[". $FromUser ."]]></FromUserName> 
                      <CreateTime>". $CreateTime ."</CreateTime>
                      <MsgType><![CDATA[link]]></MsgType>
                      <Title><![CDATA[". $Title ."]]></Title>
					  <Description><![CDATA[". $Description ."]]></Description>
					  <Url><![CDATA[". $Url ."]]></Url>
                      <MsgId>". $MessageId ."</MsgId>
                      </xml>";

		$xml = simplexml_load_string($xmlstring,'SimpleXMLElement', LIBXML_NOCDATA);

		$this->linkXmlObj = $xml;
	}
	public function getXmlObj()
	{
		return $this->linkXmlObj;
	}


}
