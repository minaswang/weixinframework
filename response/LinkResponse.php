<?php

	/**
	 * 链接消息处理
	 * @author:minas
	 * @date  : 2014-05-13
	 *
	 **/

require_once("Response.php");

class LinkResponse extends Response{

	public $_request;

	function __construct($request )
	{	
		parent::__construct($request->getRequestData()->FromUserName, $request->getRequestData()->ToUserName);
		$this->_request = $request->getRequestData();
	}


	public function run()
	{
		$this->sendText($this->_request->Title);
	}



}