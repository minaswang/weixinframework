<?php

/**
 * 处理文本消息类
 * @author: minas
 * @date  : 2014-05-13
 *
 **/
require_once("Response.php");

class TextResponse extends Response{
	public $_request;

	function __construct($request )
	{	
		parent::__construct($request->getRequestData()->FromUserName, $request->getRequestData()->ToUserName);
		$this->_request = $request->getRequestData();
	}


	public function run()
	{
		$this->sendText("收到消息：" . $this->_request->Content);
	}

	



}