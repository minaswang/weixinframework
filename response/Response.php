<?php

/**
 * 响应抽象类, 包含各种返回格式的封装
 * @author: minas
 * @date  : 2014-05-13
 *
 **/


abstract class Response {
	protected $FromUserName;
	protected $ToUserName;

	function __construct($f, $t)
	{
		$this->FromUserName = $f;
		$this->ToUserName   = $t;
	}

	private function tpl(){
        // func_get_args() 建立能够接受任何参数数目的函数,
        //此时调用tpl可以带上数目任意参数 tpl("dsaf") tpl("wang", "dsaf")
        $argv = func_get_args();
        // 特殊的类的调用方法call_user_func_array（a，b） a表示要调用的类或函数，b为调用a所需要的参数
        return call_user_func_array('sprintf', $argv);
    }

	public function sendText($content ='')
	{
		 $tpl = "<xml>
                <ToUserName><![CDATA[%s]]></ToUserName>
                <FromUserName><![CDATA[%s]]></FromUserName>
                <CreateTime>%s</CreateTime>
                <MsgType><![CDATA[text]]></MsgType>
                <Content><![CDATA[%s]]></Content>
                <FuncFlag>0</FuncFlag>
                </xml>";
        echo $this->tpl($tpl, $this->FromUserName, $this->ToUserName, time(), $content);
	}

	public function sendImage( $MediaId )
	{
		 $tpl = "<xml>
                <ToUserName><![CDATA[%s]]></ToUserName>
                <FromUserName><![CDATA[%s]]></FromUserName>
                <CreateTime>%s</CreateTime>
                <MsgType><![CDATA[image]]></MsgType>
                <Image>
                <MediaId><![CDATA[%s]]></MediaId>
                </Image>
                </xml>";
        echo $this->tpl($tpl, $this->FromUserName, $this->ToUserName, time(), $MediaId);
	}

	public function sendImageText($imgs , $flag=0)
	{
		$tpl = "<xml>
                 <ToUserName><![CDATA[%s]]></ToUserName>
                 <FromUserName><![CDATA[%s]]></FromUserName>
                 <CreateTime>%s</CreateTime>
                 <MsgType><![CDATA[news]]></MsgType>
                 <ArticleCount>%s</ArticleCount>
                 <Articles>%s</Articles>
                 <FuncFlag>%s</FuncFlag>
                 </xml>";
        $tpl_img = "<item>
                    <Title><![CDATA[%s]]></Title>
                    <Discription><![CDATA[%s]]></Discription>
                    <PicUrl><![CDATA[%s]]></PicUrl>
                    <Url><![CDATA[%s]]></Url>
                    </item>";
        $imgsXml = '';
        foreach ($imgs as $img) {
              $imgsXml .= $this->tpl($tpl_img, $img[0],$img[1],$img[2],$img[3]);
              
          //  $imgsXml .= call_user_func_array(array($this, 'tpl'), array_merge(array($tpl_img), array_pad((array)$img, 4, '')));
        }
        echo $this->tpl($tpl, $this->FromUserName, $this->ToUserName, time(), count($imgs), $imgsXml, $flag);
	}

	public function sendVoice($MediaId='')
	{
		 $tpl = "<xml>
                <ToUserName><![CDATA[%s]]></ToUserName>
                <FromUserName><![CDATA[%s]]></FromUserName>
                <CreateTime>%s</CreateTime>
                <MsgType><![CDATA[voice]]></MsgType>
                <Voice>
                <MediaId><![CDATA[%s]]></MediaId>
                </Voice>
                </xml>";
        echo $this->tpl($tpl, $this->FromUserName, $this->ToUserName, time(), $MediaId);
	}

	public abstract function run();

}