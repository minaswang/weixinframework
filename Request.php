<?php

/**
 * 处理每次请求，解析请求数据
 * @author: minas
 * @date  ：2014-05-12
 *
 **/

require_once("./request/RequestFactory.php");

class Request {

	private $requestData;
	public function  __construct() {
      if(defined('DEBUG_MODE') && DEBUG_MODE == true) {
          $this->getFakePostData();
        } else {
       $this->getPostData();
   	  }
    }	

    private function getFakePostData()
    {
        $postData = $_POST;
        /*var_dump($postData);*/
        $requestFactory = new RequestFactory();
        $request = $requestFactory->createRequest($postData);
        $this->requestData = $request->getXmlObj();
    }


   	public function getMsgType()
   	{	
   		return $this->requestData->MsgType;
   	}

    public function getRequestData()
    {
      return $this->requestData;
    }

   	private function getPostData()
   	{
   		//$GLOBALS: a way to access variables from the global scope
        //基本上$GLOBALS['HTTP_RAW_POST_DATA'] 和 $_POST是一样的。
        //但是如果post过来的数据不是PHP能够识别的，你可以用 $GLOBALS['HTTP_RAW_POST_DATA']来接收，
        //比如 text/xml 或者 soap 等等。
        $postData = $GLOBALS["HTTP_RAW_POST_DATA"];
        $postObj = null;
        if (!empty($postData)){
            //返回一个SimpleXMLElement 的对象实例,保存了xml的信息
            $postObj = simplexml_load_string($postData, 'SimpleXMLElement', LIBXML_NOCDATA);
        }
        //$this->postObj = $postObj;
        $this->requestData =  $postObj;
   	}
}