<?php

/** 微信入口文件
 * @author： Minas
 * @date  :  2014-05-12
 *
 **/

require_once("./Request.php");
require_once("./WeixinFactory.php");

/**
 * 微信接入验证
 * 在入口进行验证而不是放到框架里验证，主要是解决验证URL超时的问题
 */
if ( $_SERVER['REQUEST_METHOD'] == 'GET' &&! empty ( $_GET ['echostr'] ) && ! empty ( $_GET ["signature"] ) && ! empty ( $_GET ["nonce"] )) {
	$signature = $_GET ["signature"];
	$timestamp = $_GET ["timestamp"];
	$nonce = $_GET ["nonce"];
	$token = "weixin";   // 在此修改你的token
	
	$tmpArr = array (
			$token,
			$timestamp,
			$nonce 
	);
	sort ( $tmpArr, SORT_STRING );
	$tmpStr = sha1 ( implode ( $tmpArr ) );
	
	if ($tmpStr == $signature) {
		echo $_GET ["echostr"];
	}
	exit ();
} else {



/* 是否采用调试模式
 * true --采用本地测试工具调试模式
 * false --实际运行模式.
 *
 **/
	define('DEBUG_MODE', false);
	$request = new Request();

	$weixinFactory = new WeixinFactory();
	$response = $weixinFactory->createResponse($request);

	$response->run();
}