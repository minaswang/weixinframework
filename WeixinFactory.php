<?php

/**
 * 微信消息类工厂.
 * @author : minas
 * @date   : 2014-05-12
 *
 **/
require_once("response/TextResponse.php");
require_once("response/ImageResponse.php");
require_once("response/VoiceResponse.php");
require_once("response/VideoResponse.php");
require_once("response/LocationResponse.php");
require_once("response/LinkResponse.php");

class WeixinFactory {

	public static function createResponse( $request )
	{
		switch ($request->getMsgType()) {
			case 'text':
				return new TextResponse($request);
				break;
			case 'image':
				return new ImageResponse($request);
				break;
			case 'voice':
				return new VoiceResponse($request);
				break;
			case 'video':
				return new VideoResponse($request);
				break;
			case 'location':
				return new LocationResponse($request);
				break;
			case 'link' :
				return new LinkResponse($request);
				break;
			default:
				return false;
				break;
		}
	} 




}